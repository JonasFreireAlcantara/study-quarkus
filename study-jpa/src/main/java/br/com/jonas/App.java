package br.com.jonas;

import java.math.BigDecimal;

import br.com.jonas.dao.CategoryDAO;
import br.com.jonas.dao.ProductDAO;
import br.com.jonas.models.Category;
import br.com.jonas.models.Product;
import br.com.jonas.utils.JPAUtils;
import jakarta.persistence.EntityManager;

public class App
{
    public static void main( String[] args )
    {
        Category category = new Category("Smartphones");

        Product cell = new Product();
        cell.setName("Moto G20");
        cell.setDescription("Very good smartphone!");
        cell.setPrice(new BigDecimal("3513.13"));
        cell.setCategory(category);

        EntityManager em = JPAUtils.getEntityManager();

        CategoryDAO categoryDAO = new CategoryDAO(em);
        categoryDAO.insert(category);

        ProductDAO productDAO = new ProductDAO(em);
        productDAO.insert(cell);

    }
}
