package br.com.jonas.dao;

import br.com.jonas.models.Product;
import jakarta.persistence.EntityManager;

public class ProductDAO {

    private EntityManager em;

    public ProductDAO(EntityManager em) {
        this.em = em;
    }

    public void insert(Product product) {
        this.em.getTransaction().begin();

        try {
            this.em.persist(product);
            this.em.getTransaction().commit();

        } catch(RuntimeException runtimeException) {
            this.em.getTransaction().rollback();
        }

    }

}
