package br.com.jonas.dao;

import br.com.jonas.models.Category;
import jakarta.persistence.EntityManager;

public class CategoryDAO {

    private EntityManager em;

    public CategoryDAO(EntityManager em) {
        this.em = em;
    }

    public void insert(Category category) {
        this.em.getTransaction().begin();

        try {
            this.em.persist(category);
            this.em.getTransaction().commit();

        } catch(RuntimeException runtimeException) {
            this.em.getTransaction().rollback();
        }
    }

}
